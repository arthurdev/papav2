$(document).ready(function() {
    $(".fancybox-thumb").fancybox({
        prevEffect	: 'elastic',
        nextEffect	: 'elastic',
        helpers	: {
            title	: {
                type: 'inside'
            },
            thumbs	: {
                width	: 50,
                height	: 50
            }
        }
    });
});