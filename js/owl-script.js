$(document).ready(function(){

  	$(".owl-carousel").owlCarousel({
  		items: 1,
  		autoplay: true,
  		loop: true,
  		autoplayTimeout: 4000,
  		autoplayHoverPause: false,
  		smartSpeed: 800
  	});
});