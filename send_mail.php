<?php 

if (isset($_POST['submit'])) {
	$nom = stripslashes(trim($_POST['nom']));
	$prenom = stripslashes(trim($_POST['prenom']));
	$email = stripslashes(trim($_POST['email']));
	$objet = stripslashes(trim($_POST['objet']));
	$message = stripslashes(trim($_POST['message']));
	$error = array();
	if(empty($nom) || empty($prenom) || empty($email) || empty($objet) || empty($message)) {
		$error[] = 'Veuillez renseigner tous les champs';
	}
	$regex_mail = '/^[-+.\w]{1,64}@[-.\w]{1,64}\.[-.\w]{2,6}$/i';
	$regex_head = '/[\n\r]/';
	if (preg_match($regex_head, $nom) 
		|| preg_match($regex_head, $prenom) 
		|| preg_match($regex_head, $email) 
		|| preg_match($regex_head, $objet)
		|| preg_match($regex_head, $message)) 
	{
		$error[] = 'En-têtes interdites dans les champs du formulaire';
	}
	if (!preg_match($regex_mail, $email)) {
		
		$error[] = 'L\'adresse '.$email.' n\'est pas valide';
	}
	if(!isset($error)) {
		$to = 'eric.bourgoin3@wanadoo.fr';
		$msg = 'Salut boulet :)'."\r\n\r\n";
		$msg = 'T\'as reçu un mail de '.$nom.''.$prenom."\r\n\r\n";
		$msg = 'Voici le message : '."\r\n";
		$msg .= '***************************'."\r\n\r\n";
        $msg .= $message."\r\n";
        $msg .= '***************************'."\r\n\r\n";
        $headers .='Content-Type: text/plain; charset="utf-8"'." "; // ici on envoie le mail au format texte encodé en UTF-8
		$headers .='Content-Transfer-Encoding: 8bit'; // ici on précise qu'il y a des caractères accentués
		$headers .= 'From: '.$nom.''.$prenom.' <'.$email.'>'."\r\n\r\n";
		if (mail($to, $objet, $msg, $headers)) {

			header("Refresh:2; url=index.php");
			
			echo 'E-mail envoyé avec succès !';

		} else {

			header("Refresh:2; url=index.php");
			
			echo 'Erreur lors de l\'envoi de l\'e-mail';
		}

	} else {
		header("Refresh:2; url=index.php");
		echo '<ul>';
		foreach ($error as $err) {
			echo '<li>';
			echo $err;
			echo '</li>';
		}
		echo '</ul>';
	}
}
?>