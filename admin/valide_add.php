<?php
session_start();

// Si l'admin n'est pas connecté, on affiche une erreur //

if(!isset($_SESSION['admin'])) {

    include_once('../inc/loader_admin.php');  

    $template = $twig->loadTemplate('log_error.twig');
    echo $template->render(array());

    header("Refresh: 2;url=connexion.php");
}

else{

    include_once('../inc/loader_admin.php');  

    $template = $twig->loadTemplate('valide_add.twig');

    //On récupère les données//
    $titre = htmlspecialchars($_POST['titre']); // Titre de la prestation //
    $page = htmlspecialchars($_POST['page']);
    $dossier = '../img/upload/'.$page.'/'; // Dossier de destination de la photo //
    $dossier_min = '../img/upload/min/'.$page.'/';
    $fichier = htmlspecialchars(basename($_FILES['image']['name'])); // Nom du fichier //
    $fichier_min = $_FILES['image']['name'];
    $taille_max = 1500000; // Taille maximum tolérée, en octets //
    $taille = filesize($_FILES['image']['tmp_name']); // Taille du fichier temporaire //
    $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' ); // Extensions valides //
    $extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  ); // Extension de l'image upload //
    $path = $dossier.$fichier;
    $path_min = $dossier_min.$fichier_min;


    // On regarde si aucun champ n'est vide //

    if(empty($titre) || empty($fichier) || empty($page)) {

        $error = '<p>Tous les champs n\'ont pas été renseignés</p>';
        header("Refresh: 2;url=form_add.php");
    }

    // On vérifie les extensions //

    if (!in_array($extension_upload,$extensions_valides)) {

        $error = '<p>Vous devez sélectionner un fichier de type .jpg, .jpeg, .png ou .gif.</p>';
        header("Refresh: 2;url=form_add.php");

    }

    // On vérifie aussi la taille du fichier //

    if($taille > $taille_max){

         $error = 'Le fichier séléctionné est trop gros';
         header("Refresh: 2;url=form_add.php");
    }

    // Si il n'y a aucune erreur, on upload et on insert les données dans la base //

    if(!isset($error)) {

        $fichier = strtolower($fichier);

        if(move_uploaded_file($_FILES['image']['tmp_name'], $path)) {

            if(copy($path, $path_min)) {

                // Upload grande image

                ini_set("gd.jpeg_ignore_warning", 1);

                $source = imagecreatefromjpeg($path);
                $destination = imagecreatetruecolor(1200, 900);
                $largeur_source = imagesx($source);
                $hauteur_source = imagesy($source);
                $largeur_destination = imagesx($destination);
                $hauteur_destination = imagesy($destination);
                imagecopyresampled($destination, $source, 0, 0, 0, 0, $largeur_destination, $hauteur_destination, $largeur_source, $hauteur_source);
                imagejpeg($destination, $path);

                ini_set("gd.jpeg_ignore_warning", 1);

                $source_min = imagecreatefromjpeg($path_min);
                $destination_min = imagecreatetruecolor(300, 225); 
                $largeur_source_min = imagesx($source_min);
                $hauteur_source_min = imagesy($source_min);
                $largeur_destination_min = imagesx($destination_min);
                $hauteur_destination_min = imagesy($destination_min);
                imagecopyresampled($destination_min, $source_min, 0, 0, 0, 0, $largeur_destination_min, $hauteur_destination_min, $largeur_source_min, $hauteur_source_min);
                imagejpeg($destination_min, $path_min);

                require_once('../inc/bdd.php');

                $request = $bdd->prepare("INSERT INTO photo (titre, image, page) VALUES (:titre, :image, :page)");
                $request->execute(array("titre" => $titre,
                                        "image" => $fichier,
                                        "page" => $page
                                        ));

                header("Refresh: 2;url=photos.php?page=".$page);
                echo '<div class="container">';
                echo '<div class="row">';
                echo '<div class="valide ">';
                echo '<h1>Photo ajoutée !</h1>';
                echo '<p>Vous allez être redirigé vers la liste des photos</p>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

            } else {

                header("Refresh: 2;url=form_add.php");
                echo '<div class="container">';
                echo '<div class="row">';
                echo '<div class="valide ">';
                echo '<h1>Erreur !</h1>';
                echo '<p>Erreur lors du transfert de fichier</p>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

            }

        }

        else {

            header("Refresh: 2;url=form_add.php");
            echo '<div class="container">';
            echo '<div class="row">';
            echo '<div class="valide ">';
            echo '<h1>Erreur !</h1>';
            echo '<p>Erreur lors du transfert de fichier</p>';
            echo '</div>';
            echo '</div>';
            echo '</div>';

        }
    }

    else {

        header("Refresh: 2;url=form_add.php");
        echo '<div class="container">';
        echo '<div class="row">';
        echo '<div class="valide ">';
        echo '<h1>Erreur !</h1>';
        echo $error;
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }

        echo $template->render(array());
}

?>