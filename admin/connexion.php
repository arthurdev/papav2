<?php
session_start();

    // On vérifie si l'utilisateur n'est oas déja connecté //

    if(isset($_SESSION['admin'])) {

        include_once('../inc/loader_admin.php');      

        header("Refresh: 2;url=index.php");

        $template = $twig->loadTemplate('error_connexion.twig');
        echo $template->render(array());

    }

    else{

    // Si il n'est pas connecté on affiche le formulaire //

    include_once('../inc/loader_admin.php');      

    $template = $twig->loadTemplate('connexion.twig');

        // Dés qu'il valide le formulaire, on récupère les variables //

        if(isset($_POST['submit'])){

            $pseudo = $_POST['pseudo'];
            $password = md5($_POST['password']);

            require_once('../inc/bdd.php');

            $req = $bdd->prepare('SELECT admin_id, admin_pseudo, admin_password FROM admin WHERE admin_pseudo = :pseudo AND admin_password = :password');
            $req->execute(array(
            'pseudo' => $pseudo,
            'password' => $password));

            $admin = $req->fetch();

            // Si les ID ne correspondent pas, on affiche un message d'erreur //

            if(!$admin){

                echo '<div class="container">';
                echo '<div class="row">';
                echo '<h4>Mauvais identifiant ou mot de passe.</h4>';
                echo '</div>';
                echo '</div>';

            }

            // Si tout est bon on commence la session //

            else{
                $_SESSION['admin'] = 1;
                header("Location: index.php");
            }
        }
        
        echo $template->render(array());
    }
?>
