<?php
session_start();

    // On vérifie si l'utilisateur n'est oas déja connecté //

    if(!isset($_SESSION['admin'])) {

        include_once('../inc/loader_admin.php');      

        header("Refresh: 2;url=connexion.php");

        $template = $twig->loadTemplate('log_error.twig');
        echo $template->render(array());

    }

    else{

    	include_once('../inc/loader_admin.php');      

    	$template = $twig->loadTemplate('index.twig');
    	echo $template->render(array());
    }