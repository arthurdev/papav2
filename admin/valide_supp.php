<?php 
session_start();

    // Si l'admin n'est pas connecté, on affiche une erreur //

    if(!isset($_SESSION['admin'])) {

        include_once('../inc/loader_admin.php');  

        $template = $twig->loadTemplate('log_error.twig');
        echo $template->render(array());

        header("Refresh: 2;url=connexion.php");
    }

    // Sinon, on continue //

    else{

        include_once('../inc/loader_admin.php');  

        $template = $twig->loadTemplate('valide_supp.twig');     

        $id = intval($_GET['id']);


        require_once('../inc/bdd.php');

        // Requête de suppression //
        
        $request = $bdd->query("SELECT image, page FROM photo WHERE id = ".$id);
        $result = $request->fetch();

        if(isset($result)) {

            if (file_exists('../img/upload/'.$result['page'].'/'.$result['image'])) {
                

                unlink('../img/upload/'.$result['page'].'/'.$result['image']);

            }

            if (file_exists('../img/upload/min/'.$result['page'].'/'.$result['image'])) {
                

                unlink('../img/upload/min/'.$result['page'].'/'.$result['image']);
                
            }

        }

        if ($request = $bdd->query("DELETE FROM photo WHERE id = ".$id)) {

            header("Refresh: 2;url=photos.php?page=".$result['page']);
            echo '<div class="container">';
            echo '<div class="row">';
            echo '<div class="valide">';
            echo '<h1>Photo supprimée !</h1>';
            echo '<p>Vous allez être redirigé vers la liste des photos</p>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }

        else{

            header("Refresh: 2;url=photos.php?page=".$_result['page']);
        	echo '<div class="container">';
            echo '<div class="row">';
            echo '<div class="valide">';
            echo '<h1>Erreur !</h1>';
            echo '<p>Vous allez être redirigé vers la liste des photos</p>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        }

        echo $template->render(array()); 
    }

?>
