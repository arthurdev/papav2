<?php
session_start();

if(!isset($_SESSION['admin'])) {

    include_once('../inc/loader_admin.php');  

    $template = $twig->loadTemplate('log_error.twig');
    echo $template->render(array());
    header("Refresh: 2;url=connexion.php");
}

else{

    include_once('../inc/loader_admin.php');  

    $template = $twig->loadTemplate('valide_modif.twig');

    //On récupère les données//
    $id = intval($_GET['id']);
    $titre = htmlspecialchars($_POST['titre']); // Titre de la prestation //
    $page = htmlspecialchars($_POST['page']);

    // On effectue les vérifications //

    // On regarde si aucun champ n'est vide //

    if(empty($titre) || empty($page)) {

        $error = '<p>Tous les champs n\'ont pas été renseignés</p>';
        header("Refresh: 2;url=form_modif.php?id=".$id);
    }

    // Si il n'y a aucune erreur, on upload et on insert les données dans la base //

    if(!isset($error)) {


            require_once('../inc/bdd.php');

            $insert = $bdd->prepare("UPDATE photo
                                    SET titre = :titre,
                                        page = :page
                                    WHERE id = :id");

            $insert->execute(array("titre" => $titre,
                                    "page" => $page,
                                    "id" => $id
                                    ));

            header("Refresh: 2;url=photos.php?page=".$page);
            echo '<div class="container">';
            echo '<div class="row">';
            echo '<div class="valide ">';
            echo '<h1>Modification effectuée !</h1>';
            echo '<p>Vous allez être redirigé vers la liste des photos</p>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
    }

    else {

        header("Refresh: 2;url=form_modif.php?id=".$id);
        echo '<div class="container">';
        echo '<div class="row">';
        echo '<div class="valide ">';
        echo '<h1>Erreur !</h1>';
        echo $error;
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }

        echo $template->render(array());
}

?>