<?php
session_start();
    
    // Si l'admin n'est pas connecté, on affiche pas le formulaire //

    if(!isset($_SESSION['admin'])) {

        include_once('../inc/loader_admin.php');      

        $template = $twig->loadTemplate('log_error.twig');
        echo $template->render(array());

        header("Refresh: 2;url=connexion.php");
    }

    // Si il est bien connecté, on affiche le template //

    else{

        include_once('../inc/loader_admin.php');      

        $template = $twig->loadTemplate('form_add.twig');
        echo $template->render(array());
    }

?>
