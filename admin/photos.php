<?php
   
session_start();

    // Si l'admin n'est pas connecté, on affiche pas le contenu //

    if(!isset($_SESSION['admin'])) {

        include_once('../inc/loader_admin.php');  

        $template = $twig->loadTemplate('log_error.twig');
        echo $template->render(array());      

        header("Refresh: 2;url=connexion.php");
    }

    // S'il est bien connecté, on affiche //
    
    else{

        include_once('../inc/loader_admin.php');   
        require_once('../inc/bdd.php');

        $page_res = htmlspecialchars($_GET['page']);


        // On effectue la requête //

        $request = $bdd->prepare('SELECT * FROM photo WHERE page = :page ORDER BY id DESC');
        $request->bindValue(':page', $page_res, PDO::PARAM_STR);
        $request->execute();
        $result = $request->fetchAll();

        // On affiche le template en faisant passer les variables //

        $template = $twig->loadTemplate('photos.twig');
        echo $template->render(array(
            'photos' => $result,
            'page' => $page_res
        ));        

    }
?>