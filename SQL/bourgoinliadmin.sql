-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Client :  bourgoinliadmin.mysql.db
-- Généré le :  Dim 27 Novembre 2016 à 12:46
-- Version du serveur :  5.5.52-0+deb7u1-log
-- Version de PHP :  5.4.45-0+deb7u5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bourgoinliadmin`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_pseudo` varchar(15) NOT NULL,
  `admin_password` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='administrateur';

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_pseudo`, `admin_password`) VALUES
(1, 'ericbourgoin', 'edba756a06cb9dfb26369b6d82be76dd');

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` int(11) NOT NULL,
  `titre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `photo`
--

INSERT INTO `photo` (`id`, `titre`, `image`, `page`) VALUES
(3, 'Meuble après restauration', '24.jpg', 'restauration'),
(6, 'Restauration d''un bureau Louis XVI en acajou époque XIVème', '21.jpg', 'restauration'),
(7, 'Table à volets 4 pieds en noyer', '20.jpg', 'restauration'),
(8, 'Cabinet Louis XIII en ébène', '19.jpg', 'restauration'),
(9, 'Cave à liqueur époque Napoléon III', '18.jpg', 'restauration'),
(10, 'Buffet en noyer époque XVIII ème siècle', '17.jpg', 'restauration'),
(13, 'Ensemble de coffrets marqueterie', '14.jpg', 'restauration'),
(14, 'Commode époque XVIII ème palissandre et bois de rose', '13.jpg', 'restauration'),
(15, 'Commode époque XVIII ème en palissandre', '12.jpg', 'restauration'),
(16, 'Commode époque XVIII ème en noyer', '11.jpg', 'restauration'),
(17, 'Commode époque XVIII ème en chêne éclairci', '10.jpg', 'restauration'),
(19, 'Bureau à dos d''âne, époque du XVIII ème en noyer', '8.jpg', 'restauration'),
(20, 'Commode époque Louis XVI bois de rose', '7.jpg', 'restauration'),
(21, 'Bureau XVIII ème siècle plaquage amarante', '6.jpg', 'restauration'),
(22, 'Commode époque XVIII ème siècle, plaquage palissandre', '5.jpg', 'restauration'),
(23, 'Commode époque XVIII ème siècle, plaquage palissandre', '4.jpg', 'restauration'),
(24, 'Commode époque Louis XVI en noyer', '3.jpg', 'restauration'),
(25, 'Bureau Louis XV à dos d''âne en merisier', '2.jpg', 'restauration'),
(27, 'Fabrication d''un retable dans une chapelle privée', '12.jpg', 'fabrication'),
(28, 'Meuble de salle de bain en frêne avec finition cérusée', '10.jpg', 'fabrication'),
(29, 'Table en noyer massif', '8.jpg', 'fabrication'),
(30, 'Meuble en chêne pour synthétiseur avec quatre tiroirs', '7.jpg', 'fabrication'),
(31, 'Meuble en chêne pour synthétiseur avec quatre tiroirs', '9.jpg', 'fabrication'),
(32, 'Lit en noyer massif', '6.jpg', 'fabrication'),
(33, 'Buffet quatre portes et quatre tiroirs en chêne massif', '5.jpg', 'fabrication'),
(34, 'Table ovale six pieds avec allonges en noyer massif', '4.jpg', 'fabrication'),
(35, 'Ensemble de six banquettes en hêtre de style Louis XVI', '3.jpg', 'fabrication'),
(36, 'Table de salon', '11.jpg', 'fabrication'),
(37, 'Lit deux places en noyer massif', '2.jpg', 'fabrication'),
(39, 'Bureau époque Louis XV plaquage palissandre', '1.jpg', 'restauration'),
(40, 'Bar et bilbiothèque en noyer massif', '1.jpg', 'fabrication'),
(41, 'Cuisine aménagée en chêne avec plan de travail stratifié', '10.jpg', 'cuisine'),
(43, 'Cuisine aménagée finition laquée, plan de travail stratifié avec crédences coordonnées', '8.jpg', 'cuisine'),
(45, 'Cuisine aménagée finition laquée, plan de travail stratifié avec crédences coordonnées', '9.jpg', 'cuisine'),
(46, 'Cuisine aménagée en chêne finition laque trasparente, plan de travail en faïence', '7.jpg', 'cuisine'),
(47, 'Cuisine aménagée en hêtre vernis naturel, plan de travail en quartz', '6.jpg', 'cuisine'),
(48, 'Cuisine aménagée en hêtre vernis naturel, plan de travail en quartz', '5.jpg', 'cuisine'),
(49, 'Cuisine aménagée en chêne massif et plan de travail en pierre naturelle', '4.jpg', 'cuisine'),
(50, 'Cuisine aménagée en chêne massif et plan de travail en pierre naturelle', '3.jpg', 'cuisine'),
(51, 'Cuisine aménagée finition laquée, plan de travail stratifié et crédences coordonnées', '2.jpg', 'cuisine'),
(52, 'Cuisine aménagée finition laquée, plan de travail stratifié et crédences coordonnées', '1.jpg', 'cuisine'),
(53, 'Ensemble boiseries et bibliothèque', '7.jpg', 'amenagement'),
(54, 'Vue du mur à aménager', '6.jpg', 'amenagement'),
(55, 'Bibliothèque en chêne', '5.jpg', 'amenagement'),
(58, 'Aménagement bibliothèque sous l''escalier', '3.jpg', 'amenagement'),
(59, 'Placard coulissant en chêne sous un comble', '2.jpg', 'amenagement'),
(60, 'Aménagement d''un ensemble rangement', '1.jpg', 'amenagement'),
(63, 'Aménagement en chêne sous l''escalier', '4.jpg', 'amenagement');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
