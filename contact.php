<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Contact - Bourgoin Eric</title>
	<?php require_once('inc/content/meta.php'); ?>
</head>
<body id="body-contact">
<div class="container-fluid">
    <div class="row">
      <nav class="rea-navbar navbar navbar-default" id="mynavbar">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/content/logo.png" alt="logo"></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="index.php">Accueil</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Réalisations<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="restauration.php">Restauration de meuble ancien</a></li>
                  <li><a href="fabrication.php">Fabrication sur mesure</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="cuisine.php">Aménagement de cuisine</a></li>
                  <li><a href="amenagement.php">Aménagement intérieur</a></li>
                </ul>
              </li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    </div>
</div>

<div class="container" id="header-contact">
	<div class="row">
    <div class="header-padding col-md-6 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
      <div class="coordonnees">
        <h3>Mes coordonnées</h3>
        <ul>
          <li>Eric Bourgoin</li>
          <li>5 Grande Rue</li>
          <li>89300 Looze</li>
          <li>03.86.62.32.63</li>
          <li>eric.bourgoin3@wanadoo.fr</li>
        </ul>
      </div>
    </div>
    <div class="header-padding col-md-6 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
      <div class="coordonnees">
        <h3>Mentions légales et horaires</h3>
        <ul>
          <li>SIREN : 444 391 379</li>
          <li>SIRET : 444 391 379 00013</li>
          <li>Code APE : 4332A</li>
        </ul>
        <p>Je suis à votre disposition à mon atelier du <strong>lundi au samedi</strong> de <strong>8h à 12h</strong> et de <strong>13h30 à 18h</strong><br /></p>
      </div>
    </div>
	</div>
</div>

<div class="container-fluid" id="form-contact">
    <div id="formulaire-contact">
      <form class="form-horizontal" action="send_mail.php" method="post">
        <div class="form-group">
          <label class="control-label col-xs-2" for="nom">Nom :</label>
          <div class="col-xs-8">
            <input type="nom" name="nom" class="form-control" id="email" placeholder="Nom" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-xs-2" for="prenom">Prénom :</label>
          <div class="col-xs-8">
            <input type="prenom" name="prenom" class="form-control" id="email" placeholder="Prénom" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-xs-2" for="email">Email :</label>
          <div class="col-xs-8">
            <input type="email" name="email" class="form-control" id="email" placeholder="Email" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-xs-2" for="objet">Objet :</label>
          <div class="col-xs-8"> 
            <input type="objet" name="objet" class="form-control" id="objet" placeholder="Objet" required>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-xs-2" for="message">Message :</label>
          <div class="col-xs-8">
            <textarea class="form-control" name="message" rows="5" id="message" placeholder="Message"></textarea>
          </div>
        </div>
        <div class="form-group"> 
          <div class="col-xs-offset-2 col-xs-10">
            <button type="submit" class="btn btn-default">Envoyer</button>
          </div>
        </div>
      </form>
    </div>  
</div>

<div class="container-fluid" id="find-me">
  <div class="row">
    <h2 class="col-xs-12 text-center">Me trouver</h2>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4575.202322933459!2d3.4744715700337077!3d47.94892411302316!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47efaad434ee244b%3A0xac764e883e8a8852!2sBourgoin+Eric!5e0!3m2!1sfr!2sfr!4v1479462063881" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>


<?php require_once('inc/content/footer.php') ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59671380-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>