<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Restauration - Bourgoin Eric</title>
	<?php require_once('inc/content/meta.php');?>
</head>
<body>
	<div class="container-fluid">
    <div class="row">
      <nav class="rea-navbar navbar navbar-default" id="mynavbar">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/content/logo.png" alt="logo"></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="index.php">Accueil</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Réalisations<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Restauration de meuble ancien</a></li>
                  <li><a href="fabrication.php">Fabrication sur mesure</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="cuisine.php">Aménagement de cuisine</a></li>
                  <li><a href="amenagement.php">Aménagement intérieur</a></li>
                </ul>
              </li>
              <li><a href="contact.php">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    </div>
  </div>

  <div class="container-fluid">
    <div class="row">
      <div class="banner-rea" id="banner-restauration">
        <h1 class="col-xs-12">Restauration <p>de meubles anciens</p></h1>
      </div>
    </div>
  </div>

  <?php include_once('inc/bdd.php'); ?>

  <div class="container">
    <div class="row">
      <div class="photos-content">
  <?php 

    $req = $bdd->prepare("SELECT titre, image FROM photo WHERE page ='restauration' ORDER BY id DESC");

    $req->execute();

    $result = $req->fetchAll();

    foreach ($result as $photo) {

  ?>

    <div class="col-xs-6 col-sm-4 col-md-3">
      <a class="fancybox-thumb" rel="fancybox-thumb" href="img/upload/restauration/<?php echo $photo['image']; ?>" title="<?php echo $photo['titre']; ?>">
        <img class="img-responsive" src="img/upload/min/restauration/<?php echo $photo['image']; ?>" alt="<?php echo $photo['titre']; ?>" />
      </a>
    </div>


  <?php }; ?>

      </div>
    </div>
  </div>

<?php require_once('inc/content/footer.php'); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59671380-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>

