<!DOCTYPE html>
<html lang="fr">
<head>
	<title>Bourgoin Eric - Accueil</title>
  <?php require_once('inc/content/meta.php'); ?>
  <link rel="stylesheet" type="text/css" href="lib/owl-carousel/assets/owl.carousel.css">
  <link rel="stylesheet" type="text/css" href="lib/owl-carousel/assets/owl.theme.default.css">
  <link rel="stylesheet" type="text/css" href="lib/owl-carousel/assets/animate.css">
</head>
<body>

<div class="container-fluid">
    <div class="row">
      <nav class="navbar navbar-default" id="mynavbar" >
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="img/content/logo.png" alt="logo"></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="#">Accueil</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Réalisations<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="restauration.php">Restauration de meuble ancien</a></li>
                  <li><a href="fabrication.php">Fabrication sur mesure</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="cuisine.php">Aménagement de cuisine</a></li>
                  <li><a href="amenagement.php">Aménagement intérieur</a></li>
                </ul>
              </li>
              <li><a href="contact.php">Contact</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
			<div class="owl-carousel owl-theme" style="heigth: 300px">
          <div><div class="text-overlay"><h1 class="col-md-10 col-md-offset-1">Eric Bourgoin vous souhaite la bienvenue</h1></div><img class="animated owl-item" src="img/slider/1.jpg" alt="slide-item"></div>
			  	<div><div class="text-overlay"><h2 class="col-md-10 col-md-offset-1">Ebéniste - menuisier professionnel</h2></div><img class="animated owl-item" src="img/slider/2.jpg" alt="slide-item"></div>
          <div><div class="text-overlay"><h2 class="col-md-10 col-md-offset-1">Artisan à son compte depuis 2003</h2></div><img class="animated owl-item" src="img/slider/3.jpg" alt="slide-item"></div>
			</div>
		</div>
	</div>

<div class="container" id="header-content">
  <div class="row">
    <div class="header-padding col-md-8 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
        <div class="header-contain">
          <h3>Présentation</h3>
          <p>Bonjour et bienvenue dans mon espace numérique. Vous trouverez sur ce site un <strong>aperçu de mes réalisations</strong>, toutes élaborées dans le respect du travail bien fait, et dans l'objectif de <strong>satisfaire pleinement les clients</strong> qui m'ont fait confiance. Je serai heureux de vous accueillir à mon atelier pour discuter ensemble de vos futurs projets.</p>
        </div>
        <div class="header-contain" id="bottom-left-header">
          <h3>Ebéniste diplômé</h3>
          <p>École BOULLE - Paris XIIème :
            <ul>
              <li><strong>Diplôme des Métiers d'Art</strong> (DMA) en ébénisterie obtenu en 1993</li>
              <li><strong>Brevet de Technicien</strong> (BT) en agencement obtenu en 1990</li>
            </ul>
          </p>
          <p>Lycée professionnel de Fourchambault :
            <ul>
              <li>BEP <strong>Bois et Matériaux Associés</strong> obtenu en 1998</li>
              <li>1er prix départemental et 1er prix régional de la <strong>Société d'Encouragement aux Métiers d'Art</strong> (SEMA) en 1988</li>
            </ul>
          </p>
        </div>
    </div>
    <div class="header-padding col-md-4 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1" id="right-header">
      <div class="header-contain">
        <h3>Ebéniste expérimenté</h3>
        <p>Après l'obtention du <strong>DMA en 1993</strong> j'ai alors débuté comme salarié chez un ébéniste près de Sens jusqu'en 2002. En 2003 j'ai décidé de créer ma propre <strong>entreprise artisanale à Looze</strong>. Je suis à votre disposition pour :
        <ul>
          <li><strong>La restauration de vos meubles anciens</strong>, tout en gardant leur authenticité et en respectant les méthodes des anciens ébénistes.</li>
          <li>La fabrication de projets de mobilier et de <strong>menuiserie intérieure sur mesure</strong>.</li>
          <li>La réalisation de vos <strong>projets d'aménagement intérieur</strong>.</li>
        </ul>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row">
    <div id="paralax-quote">
      <div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1" id="contactme">
        <h2><strong>Contactez-moi</strong></h2>
        <p>Pour votre projet</p>
        <div class="paralax-logo">
          <hr>
          <img src="img/content/logo.png" alt="logo-paralax">
          <hr>
        </div>
        <a href="contact.php"><button class="btn btn-default">ME CONTACTER</button></a>
      </div>
    </div>
  </div>
</div>

<div class="container" id="all-works">
  <div class="row">
    <h2 class="col-xs-12 text-center">Mes réalisations</h2>
    <a href="restauration.php"><div class="work-container col-md-5 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1" id="container-rest">
      <h3>Restauration de meubles anciens</h3>
    </div></a>
    <a href="fabrication.php"><div class="work-container col-md-5 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1" id="container-fab">
      <h3>Fabrication sur mesure</h3>
    </div>
  </div></a>
  <div class="row">
    <a href="cuisine.php"><div class="work-container col-md-5 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1" id="container-cuis">
      <h3>Aménagement de cuisine</h3>
    </div></a>
    <a href="amenagement.php"><div class="work-container col-md-5 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1" id="container-amg">
      <h3>Aménagement intérieur</h3>
    </div></a>
  </div>
</div>

<?php require_once('inc/content/footer.php'); ?>

<script type="text/javascript" src="lib/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/owl-script.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59671380-2', 'auto');
  ga('send', 'pageview');

</script>
</body>

</html>