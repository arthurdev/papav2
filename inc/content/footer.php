<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-5" id="footer-left">
        <ul>
          <li><a href="#">5 Grande Rue, 89300 Looze</a></li>
          <li><a href="tel:+33386623263">03 86 62 32 63</a></li>
          <li><a href="mailto:eric.bourgoin3@wanadoo.fr">eric.bourgoin3@wanadoo.fr</a></li>
        </ul>
      </div>
      <div class="col-md-4" id="footer-logo">
        <a href="index.php"><img src="img/content/logo.png" alt="logo"></a>
      </div>
      <div class="col-md-4 col-sm-6 col-xs-7" id="footer-right">
        <ul>
          <li><a href="index.php">Accueil</a></li>
          <li><a href="restauration.php">Restauration de meubles anciens</a></li>
          <li><a href="fabrication.php">Fabrication sur mesure</a></li>
          <li><a href="cuisine.php">Aménagement de cuisine</a></li>
          <li><a href="amenagement.php">Aménagement intérieur</a></li>
          <li><a href="contact.php">Contact</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row">
      <div class="" id="copy">
        <p>Copyright &copy Eric Bourgoin -  All Rights Reserved</p>
      </div>
    </div>
  </div>
</footer>